# -*- coding: utf-8 -*-
"""Euclidean Algorithm

This algorithm is based of the description provided at:
https://en.wikipedia.org/wiki/Euclidean_algorithm

The goal of this file is to provide an efficient library for computing
the greate common denominator of two numbers. Additionally, this will
enable to efficient computation of modulo for future projects.

This file is written for python 3.4.3.

Author: Brandon Mendrick
File available under GNU GPL.

"""

import time
import math

class EuclidAlg:
	"""Module for computing greatest common factors.

	initialization can take two parameters and calculate the gcf.

	compute() is the main interface for determining gcf
		called with two parameters and returns an integer

	"""

	def __init__(self, a=None, b=None):
		"""Initialization of the class and handling of initial input computation."""
		if a and b:
			self.compute(a, b)
		elif (a and not b) or (b and not a):
			return 0

		return

	def compute(self, a, b):
		"""Compute the greatest common factor of a and b."""
		if a == b:
			return a

		if a < b:
			# reversing the order of the variables
			tmp = a
			a = b
			b = tmp

		while True:
			q = a // b
			r = a - (b * q)

			if r == 0:
				return b

			else:
				a = b
				b = r

def main():
	"""Main function for testing purposes."""
	alg = EuclidAlg()
	print(alg.compute(1071, 462))

if __name__ == "__main__":
	main()
