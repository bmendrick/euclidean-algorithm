#Euclidean Algorithm

This is an implementation of the Eucliden algorithm described here (https://en.wikipedia.org/wiki/Euclidean_algorithm).

This implementation is in Python and exists under the GNU GPL.
